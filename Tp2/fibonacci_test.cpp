#include "fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Groupfibonacci) {};
TEST(Groupfibonacci, test_fibonacci_rec ){
    int listresult[]={0,1,1,2,3};
    for(int i=0;i<5;i++){
        CHECK_EQUAL(listresult[i],fibonacci_Rec(i))
    }
}

TEST(Groupfibonacci, test_fibonacci_ite){
    int listresult[]={0,1,1,2,3};
    for(int i=0;i<5;i++){
        CHECK_EQUAL(listresult[i],fibonacci_Rec(i))
    }
}
