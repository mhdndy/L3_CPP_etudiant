#ifndef VECTEUR3_HPP
#define VECTEUR3_HPP

struct vecteur3d{
   float x,y,z;
   void affichervect(const vecteur3d &v);
};

void afficher(const vecteur3d &v);
float norme(const vecteur3d &v);
float produit_scalaire(const vecteur3d &v1,const vecteur3d &v2);
float addition(const vecteur3d &v1,const vecteur3d &v2);
#endif // VECTEUR3_HPP
