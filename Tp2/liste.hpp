#ifndef LISTE_HPP
#define LISTE_HPP

struct Noeud{
    int valeur;
    Noeud*suivant;
};

struct liste{
    Noeud*tete;
    liste Liste();
    void ajouerDevant(int valeur);
    void afficher();
    int const getTaille();
    int const getElement(int indice);
};

#endif //liste.hpp
