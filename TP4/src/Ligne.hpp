#ifndef LIGNE_H
#define LIGNE_H

#include <string>
#include <iostream>

#include "FigureGeometrique.hpp"
#include "Point.hpp"
#include "Couleur.hpp"

class Ligne : public FigureGeometrique {
	private:
			Point _p0 ,_p1 ;
	public:
			Ligne(const Couleur & couleur, const Point & p0 ,const Point & p1 ) ;
			Point getP0() const ;
			Point getP1() const ;
			void afficher() const override;
} ;
#endif    
