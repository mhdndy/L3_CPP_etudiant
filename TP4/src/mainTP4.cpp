#include <string>
#include <iostream>
#include <vector>

#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

using namespace std ;
int main() {
	Point  p0 , p1 ;
	p0._x= 10 ; p0._y= 10 ;
	p1._x= 20 ; p1._y= 20 ;
	
	Couleur C ;
	C._r = 1 ; C._g = 0 ; C._b = 0 ;
	
	//Ligne ligne(C , p0 ,p1 ) ;
	//ligne.afficher() ;
	
	Point  center ;
	center._x= 100 ; center._y= 200 ;
	
	
	//PolygoneRegulier pentagone(C, center , 50 ,5 ) ;
	//pentagone.afficher() ;
	
		
	vector<FigureGeometrique * >  figures {
		new Ligne(C , p0 ,p1 ) ,
		new PolygoneRegulier (C, center , 50 ,5 ), 
		new PolygoneRegulier (C, center , 10 ,5 )
	};
	for(FigureGeometrique * ptrFig : figures)
		ptrFig->afficher() ;
	
	for(FigureGeometrique * ptrFig : figures)
		delete ptrFig ;
	
	
	return 0;
}
