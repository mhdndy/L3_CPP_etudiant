#ifndef POLYGONEREGULIER_H
#define POLYGONEREGULIER_H

#include <string>
#include <iostream>

#include "FigureGeometrique.hpp"
#include "Point.hpp"
#include "Couleur.hpp"

class PolygoneRegulier: public FigureGeometrique{
	private:
		int _nbPoints ;
		Point * _points ;
	public:
		PolygoneRegulier(const Couleur & couleur,const Point & centre, int rayon, int nbCotes);
		int getNbPoints() const;
		Point getPoint(int indice) const ;
		void afficher() const override;
};
#endif
