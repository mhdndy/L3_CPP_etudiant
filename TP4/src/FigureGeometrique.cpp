#include <iostream>
#include <string>
#include "FigureGeometrique.hpp"

using namespace std ;

FigureGeometrique::FigureGeometrique(const  Couleur & couleur) :
_couleur(couleur) {}

Couleur FigureGeometrique::getCouleur() const{
	return _couleur ;
}
