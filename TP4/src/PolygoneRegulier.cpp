#include <string>
#include <iostream>
#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>

#include "PolygoneRegulier.hpp"
using namespace std ;

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur,const Point & centre, int rayon, int nbCotes):
 FigureGeometrique(couleur), _nbPoints(nbCotes) {
	_points = new Point[_nbPoints] ;
	for(int i = 0 ; i < _nbPoints ;i++ ){
		Point pt;
		float theta = i * 2 * M_PI /nbCotes ;
		pt._x = centre._x + rayon * cos(theta);
		pt._y = centre._y + rayon * sin(theta);
		
		_points[i] = pt ;
	}
}
int PolygoneRegulier::getNbPoints() const{
	return _nbPoints ;
}

Point PolygoneRegulier::getPoint(int indice ) const{
	return _points[indice] ;
}

void PolygoneRegulier::afficher()const{
	cout<<"PolygoneRegulier "<<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b<<"   " ;
	for(int i = 0 ; i < _nbPoints ;i++ ){
		cout<<_points[i]._x<<"_"<<_points[i]._y<<"  " ;
	}
	cout<<endl<<endl ;
}
