#ifndef FIGUREGEOMETRIQUE_H
#define FIGUREGEOMETRIQUE_H
#include <string>

#include "Couleur.hpp"

using namespace std ;

class FigureGeometrique {
	 private:
			 Couleur _couleur ;
	 public:
			FigureGeometrique(const Couleur & couleur ) ;
			Couleur getCouleur() const ;
			virtual void afficher() const =0;
};
#endif
