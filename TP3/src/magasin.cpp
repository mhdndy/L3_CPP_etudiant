#include "magasin.h"
#include<iostream>


Magasin::Magasin():
    _id_courantClient(0),_id_courantProduit(0)
{}

int Magasin::nbClient()const{
    return _clients.size() ;
}

void Magasin::ajouterClient(const string & nom ){
    Client cli(_id_courantClient++ , nom) ;
    _clients.push_back(cli);
}

void Magasin::afficherClients() const {
    for(size_t i=0  ; i<_clients.size();i++){
     _clients[i].afficherClient() ;
    }
}

void Magasin::supprimerClient(int _idClient){
    for(int i=0;i<_clients.size();i++){
        if(_clients[i].getId()==_idClient){
            swap(_clients[_clients.size()-1],_clients[i]);
            _clients.pop_back();
        }
    }
}
