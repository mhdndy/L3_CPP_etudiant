#include "client.h"
#include<iostream>
Client::Client(int _id , const string & _nom ):
    _id(_id) , _nom(_nom)
{}

int Client::getId() const{
    return _id ;
}

const string & Client::getNom() const {
    return _nom ;
}

void Client::afficherClient() const{
cout<<"Client("<<_id<<", "<<_nom<<")"<<endl ;
}
