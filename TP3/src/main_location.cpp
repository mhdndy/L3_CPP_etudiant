#include "location.h"
#include "client.h"
#include "produit.h"
#include "magasin.h"

#include <iostream>
using namespace std ;
int main() {
    Location loc(0,2) ;
    loc.afficherLocation() ;
    Client cli(42,"tata");
    cli.afficherClient() ;
    Produit pro(001 ,"alfa romeo") ;
    pro.afficherProduit();

    Magasin mag;
    mag.nbClient();
    mag.ajouterClient("mhd");
    mag.ajouterClient("libasse");
    mag.afficherClients();
    mag.supprimerClient(1);
	mag.afficherClients();
   return 0;
}

