#ifndef MAGASIN_H
#define MAGASIN_H
using namespace std ;
#include<string>
#include<vector>
#include"client.h"
#include"produit.h"
#include"location.h"

class Magasin
{
private:
    vector<Client>_clients ;
    vector<Produit>_produits ;
    vector<Location>_locations ;
    int _id_courantClient ;
    int _id_courantProduit ;
public:
    Magasin();
    int nbClient() const;
    void ajouterClient(const string & nom ) ;
    void afficherClients() const ;
    void supprimerClient(int _idClient) ;
};

#endif // MAGASIN_H
