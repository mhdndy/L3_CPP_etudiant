#ifndef PRODUIT_H
#define PRODUIT_H
#include<string>
 using namespace std ;
class Produit
{
private:
    int _id;
    string _description;
public:
    Produit(int id, const string& description);
    int getId()const;
    const string& getDescription()const;
    void afficherProduit()const;
};
#endif // PRODUIT_H
