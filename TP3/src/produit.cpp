#include<iostream>
#include "produit.h"

Produit::Produit(int id, const string& description):
_id(id) , _description(description)
{}
int Produit::getId()const{
    return _id;
}
const string& Produit::getDescription()const{
    return _description;
}
void Produit::afficherProduit()const{
    cout<<"Client("<<getId()<<","<<getDescription()<<")"<<endl;
}
