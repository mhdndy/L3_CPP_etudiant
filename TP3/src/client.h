#ifndef CLIENT_H
#define CLIENT_H
using namespace std ;
#include<string>

class Client
{
private:
    int _id ;
    string  _nom ;
public:
    Client(int _id , const string & _nom );
    int getId() const ;
    const string& getNom() const ;
    void afficherClient() const ;
};

#endif // CLIENT_H
